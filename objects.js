/* Objects are data structures where information is represeneted in form of key value pair.
   Key represents the name and value represent what is pointed by that key. Value can be anything number,
   string, function, array, another object etc.
   Functions inside objects are called method.
   Objects are generally used to store related data together.
*/

const fruits = {
    apple: 'red',
    banana: 'yellow',
    grapes: 'green',
    watermellon: 'red',
    orange : 'orange',
    quantity: 10,
    eat: function(){ return  'Eat the fruit'},
    amount:[2,2,2,2,2],
}

console.log('object: ', fruits)
console.log('Object Keys: ', Object.keys(fruits))
console.log('Executing fruits objects eat', fruits.eat())