const pushToGit = (files, commitMessage) => {     // files and commitMessage are parameters here
    let instructions = ''
    instructions += '1. Add files to git: git add ' + files
    instructions += ('\n 2. Commit the files to git with a message: git commit -m ' + commitMessage)
    instructions += ('\n 3. Push on git: git push')
    return instructions
}

console.log(pushToGit('abc.txt', 'added abc file'))  // here while calling the function these are arguments