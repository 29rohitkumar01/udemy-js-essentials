/* Polymorphism: When an organism or inorganic object or material which takes different form.
   
*/

// In below example of string concatenation age is a number but when concatenated with name it becomes string
// i.e it changes form from number to string (implicit type conversion)s
const age = 23
const name = 'Rohit'
const concatenated = name + age
console.log('conactenated: ', concatenated)