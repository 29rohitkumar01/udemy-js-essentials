/* Primitive Data types are the lowest level data value representation of the javascript language i.e they are immutable
These values are the ones that are not objects and doesn't have any objects.
Primitive Data Types: string, number, boolean, undefined, bigint, symbol.
null is a special case representation of the object and hence not a primitive.
Except undefined all primitives have there non-primitives representation: String, Number, Boolean, BigInt Symbol
*/

// string
const name = 'Rohit'
console.log('string-> Name: ', name)

// number
const age = 23
const height = 1.69
console.log('number-> Age: ', age, ' Height: ', height)

// boolean
const playBadminton = true
const playHockey = false
console.log('boolean-> Plays Badminton: ', playBadminton, ' Plays Hockey: ', playHockey)

// undefined
var income
console.log('undefined-> variables thar not assigned any value are by default undefined. Ex: income=', income)

// bigint
const stars = 1000000000000n
console.log('bigint are defined by appending n at the end. Ex. starts:', stars)

// symbol
const symbol = Symbol('abc')
console.log('Symbol are always unique.', symbol)